﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Lab1._2
{
    class One
    {
        public int A { get; set; }
        public int B { get; set; }
        public One(int a, int b)
        {
            this.A = a;
            this.B = b;
        }

        public virtual void Do()
        {
            int temp = A + B;
            return;
        }

        public virtual void Make()
        {
            int temp = A + B;
            return;
        }
    }

    class Two : One
    {
        public Two(int a, int b) : base(a, b)
        {
        }

        public override void Do()
        {
            int temp = A + B + 1;
            return;
        }

        public override void Make()
        {
            int temp = A + B + 1;
            return;
        }
    }

    class Three : Two
    {
        public Three(int a, int b) : base(a, b)
        {
        }

        public override void Do()
        {
            int temp = A + B + 2;
            return;
        }
        public override void Make()
        {
            int temp = A + B + 2;
            return;
        }
    }

    interface IOne
    {
        public int A { get; set; }
        public int B { get; set; }

        public void Do();
    }

    interface ITwo: IOne
    {
        public void Make();
    }

    class InterfaceClass: ITwo
    {
        public int A { get; set; }
        public int B { get; set; }
        public InterfaceClass(int a, int b)
        {
            A = a;
            B = b;
        }

        public void Do()
        {
            int temp = A + B + 2;
            return;
        }
        public void Make()
        {
            int temp = A + B + 2;
            return;
        }
    }
        



    class Program
    {
        static void Main(string[] args)
        {
            //interf
            Stopwatch sw = new Stopwatch();
            Type bt = typeof(IOne);

            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] types = assembly.GetTypes().Where(a => bt.IsAssignableFrom(a)).ToArray();

            sw.Start();
            for (int i = 0; i < types.Length; i++)
            {
                for(int j = 0; j < 1000000; j++)
                {
                    if (types[i] == typeof(InterfaceClass))
                    {
                        var OneConstructor = types[i].GetConstructor(new[] { typeof(int), typeof(int) });
                        InterfaceClass temp = OneConstructor.Invoke(new object[] { 2, 4 }) as InterfaceClass;
                        temp.Do();
                        temp.Make();
                    }
                }
            }
            sw.Stop();

            Console.WriteLine(sw.ElapsedMilliseconds);

            //def
            sw.Reset();
            bt = typeof(One);

            assembly = Assembly.GetExecutingAssembly();
            types = assembly.GetTypes().Where(a => bt.IsAssignableFrom(a)).ToArray();

            sw.Start();
            for (int i = 0; i < types.Length; i++)
            {
                for (int j = 0; j < 1000000; j++)
                {
                    if (types[i] == typeof(One))
                    {
                        var OneConstructor = types[i].GetConstructor(new[] { typeof(int), typeof(int) });
                        One temp = OneConstructor.Invoke(new object[] { 2, 4 }) as One;
                        temp.Do();
                        temp.Make();
                    }
                    else if (types[i] == typeof(Two))
                    {
                        var OneConstructor = types[i].GetConstructor(new[] { typeof(int), typeof(int) });
                        Two temp = OneConstructor.Invoke(new object[] { 2, 4 }) as Two;
                        temp.Do();
                        temp.Make();
                    }
                    else if (types[i] == typeof(Three))
                    {
                        var OneConstructor = types[i].GetConstructor(new[] { typeof(int), typeof(int) });
                        Three temp = OneConstructor.Invoke(new object[] { 2, 4 }) as Three;
                        temp.Do();
                        temp.Make();
                    }
                }
            }
            sw.Stop();

            Console.WriteLine(sw.ElapsedMilliseconds);

        }
    }
}
