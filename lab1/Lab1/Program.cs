﻿using System;

namespace Lab1
{
    //1---------------------------------------------------
    interface IAnimal
    {
        void Move();
    }

    abstract class Mammle
    {
        public int Age;
    }

    class Cat : Mammle, IAnimal
    {
        public Cat(int age)
        {
            Age = age;
        }

        void IAnimal.Move()
        {

        }
    }

    //2---------------------------------------------------

    abstract class test1
    {
        private int one;
        private protected int two;
        protected int three;
        internal int four;
        protected internal int five;
        public int six;
    }

    class test2 : test1
    {
        test2()
        {
            one = 1;
            two = 2;
            three = 3;
            four = 4;
            five = 5;
            six = 6;
        }
    }

    //3---------------------------------------------------
    interface test31
    {
        int age { get; set; }
        void Do();//public
    }

    class test32//internal
    {
        int age;//private
        void Do() { }//private

        struct test33//private
        {

        }
    }

    //4---------------------------------------------------
    class test41
    {
        private class test42
        {
            public int a;
        }
        public class test43
        {
            public int a;
        }

        internal class test44
        {
            public int a;
        }

        protected class test45
        {
            public int a;
        }

        protected internal class test46
        {
            public int a;
        }
    }

    class test47
    {
        test41.test42 one = new test41.test42();
        test41.test43 two = new test41.test43();
        test41.test44 three = new test41.test44();
        test41.test45 four = new test41.test45();
        test41.test46 five = new test41.test46();

        void Do()
        {
            one.a = 5;
            two.a = 5;
            three.a = 5;
            four.a = 5;
            five.a = 5;
        }
    }

    //5---------------------------------------------------

    enum test5
    {
        one = 1,
        two,
        three,
        four,
        five
    };

    class test51
    {
        void Do()
        {
            test5 one = test5.one;
            test5 two = test5.three;

            test5 three = one ^ two;
            three = one | two;
            three = one & two;
            three &= one;
            three |= one;
            three ^= one;
            three = ~one;
        }
    }

    //6---------------------------------------------------
    
    interface test6
    {
        void Mind();
    }

    interface test61: test6
    {
        void Do();
    }
    interface test62: test6
    {
        void Make();
    }

    class test63 : test61, test62
    {
        public void Do()
        {

        }

        public void Make()
        {

        }

        public void Mind()
        {

        }
    }

    //7---------------------------------------------------

    class test71
    {
        public int a;
        public test71(int p)
        {
            a = p;
        }

        public virtual void Do(int a)
        {

        }
    }

    class test72: test71
    {
        public int b;
        public test72(int p, int c): this(p, c, 0)
        {
            b = c;
        }

        public test72(int bb, int ss, int ff): base(bb)
        {
            b = ss;
        }

        public override void Do(int a)
        {
            return;
        }

        public void Do(float a)
        {
            return;
        }
    }

    //8---------------------------------------------------

    class test81
    {
        public int a;
        public static int count;
        public test81(int p)//2
        {
            a = p;
        }

        static test81()//1
        {
            count = 0;
        }
    }

    //9---------------------------------------------------

    class test91
    {
        public void Do(ref int a)
        {
            a++;
        }

        public void Make(out int a)
        {
            a = 56;
            a++;
        }
    }

    //10---------------------------------------------------

    class test10
    {
        private object[] a = new object[10];
        int[] b = new int[10];
        public void Do()
        {
            for(int i = 0; i < 10; i++)
            {
                a[i] = b[i];//boxing
                b[i] = (int)a[i];//unboxing

            }
        }
    }

    //11---------------------------------------------------

    class test11
    {
        public int a { get; set; }

        public static implicit operator test11(int x)//yavne
        {
            return new test11 { a = x };
        }
        public static explicit operator int(test11 temp)//neyavne
        {
            return temp.a;
        }

        public void Do()
        {
            test11 temp = new test11 { a = 5 };

            int a = (int)temp.a;//yavne
            temp.a = a;//neyavne
        }
    }

    //12---------------------------------------------------

    public class test121 
    { 
        public virtual void Do() 
        {
            return;
        } 
    } 
    public class test122 : test121
    { 
        public override void Do() 
        {
            int i = 0;
            return;
        } 
    }

    //13---------------------------------------------------

    class test13
    {
        public int a;

        public override bool Equals(object obj)
        {
            test13 temp = (test13)obj;
            if(temp.a == this.a)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public new static bool Equals(object obj1, object obj2)
        {
            return obj1.Equals(obj2);
        }

        public override int GetHashCode() 
        { 
            return base.GetHashCode(); 
        }

        public override string ToString() 
        { 
            return $"A:{this.a}"; 
        }

        public new Type GetType() 
        { 
            return base.GetType(); 
        }

        public new static bool ReferenceEquals(object obj1, object obj2) 
        { 
            return obj1 == obj2; 
        }
        protected new object MemberwiseClone() 
        { 
            return base.MemberwiseClone(); 
        }
    }
}
